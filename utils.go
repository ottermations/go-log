package log

import (
	"path"
	"runtime"
)

func prettifyCaller(frame *runtime.Frame) (function string, file string) {
	function = path.Base(frame.Function)
	file = path.Base(frame.File)
	return
}
