# Logger for Ottermations

This repository contains a unified logger for Ottermations.

# How it works

This package manages an underlying singleton logger instance which is deemed the best for all projects and exposes an interface which will not change even if the underlying logging library changes. Details on this implementation can be found in [`./logger.go`](./logger.go)

We expose 6 different types of logging functions with appropriate formatted versions (eg. `SomeLogLevel()` will have `SomeLogLevelf()` that works like `Print` and `Printf`)

| Function | Level   | Default output | Use case                            |
| -------- | ------- | -------------- | ----------------------------------- |
| `Print`  | N/A     | `stdout`       | Program output                      |
| `Trace`  | Trace   | `stderr`       | Appplication logging at Trace level |
| `Debug`  | Debug   | `stderr`       | Application logging at Debug level  |
| `Info`   | Info    | `stderr`       | Application logging at Info level   |
| `Warn`   | Warning | `stderr`       | Application logging at Warn level   |
| `Error`  | Error   | `stderr`       | Application logging at Error level  |

The default log level is set to `LevelInfo`. To change this:

```go
log.SetLevel(log.LevelTrace)
```

The default log format is set to textual (`log.FormatText`). To change this:

```go
log.SetFormat(log.FormatJson)
```

The default log output is set to `stderr` so that output via `Print`/`Printf` can be piped to another application. To change this:

```go
log.SetOutput(os.Stdout)
```

- See [`./levels.go`](./levels.go) for more on the available log `Level`s
- See [`./formats.go`](./formats.go) for more on the available log `Format`s

# Other things this can do

## Alerts to Telegram

This feature is for scrappy projects without a proper logging system but who wants to know if things are going down. The exposed functions are `Alert` and `Alertf` which sends an alert to the configured global `AlertTarget`.

To use this, first set the following environment variables:

1. `LOG_ALERT_TELEGRAM_CHAT_ID`: the chat ID according to your Telegram Bot (must be numerical)
2. `LOG_ALERT_TELEGRAM_TOKEN`: the API token to use with the Telegram Bot API

Then in code, use:

```go
log.Alert("hello world")
```

To change where an Alert gets sent to (in future):

```go
// for an example of a not-yet-developed Discord alert...
log.AlertTarget = log.AlertTargetDiscord
```

See [`./alerts.go`](./alerts.go) for more information

## Log to a `bytes.Buffer`

Sometimes you might want to collate logs into a buffer before displaying it, for that, you can do so using:

```go
var buffer bytes.Buffer
writer := bufio.NewWriter(&buffer)
log.SetOutput(writer)
// do the printing thang
writer.Flush()
fmt.Println(buffer.String())
```

# Example

A full example can be found at [`./cmd/example`](./cmd/example).

Running the following:

```sh
$ go run ./cmd/example/ > ./test-gt
```

Gets the following output:

```
INFO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stderr] message from Info                   
INFO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stderr] message from Infof                  
WARN[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stderr] message from Warn                   
WARN[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stderr] message from Warnf                  
ERRO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stderr] message from Error                  
ERRO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stderr] message from Errorf 
```

With this in the resulting file:

```
- - - - - - - - - -
default outputs to os.Stderr (filter out with appending '2>/dev/null' to invocation)
- - - - - - - - - -
[stderr] message from Print
[stderr] message from Printf
- - - - - - - - - -
output to os.Stdout (filter out with appending '1>/dev/null' to invocation)
- - - - - - - - - -
[stdout] message from Print
[stdout] message from Printf
INFO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stdout] message from Info                   
INFO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stdout] message from Infof                  
WARN[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stdout] message from Warn                   
WARN[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stdout] message from Warnf                  
ERRO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stdout] message from Error                  
ERRO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [stdout] message from Errorf                 
- - - - - - - - - -
output to bytes.Buffer (printed using fmt.Print to os.Stdout)
- - - - - - - - - -
[custom (stdout)] message from Print
[custom (stdout)] message from Printf
INFO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [custom (stdout)] message from Info          
INFO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [custom (stdout)] message from Infof         
WARN[2022-11-19 15:56:06 +0800]main.go main.writeLogs [custom (stdout)] message from Warn          
WARN[2022-11-19 15:56:06 +0800]main.go main.writeLogs [custom (stdout)] message from Warnf         
ERRO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [custom (stdout)] message from Error         
ERRO[2022-11-19 15:56:06 +0800]main.go main.writeLogs [custom (stdout)] message from Errorf
```

# License

Code is licensed under the [MIT license](./LICENSE)
