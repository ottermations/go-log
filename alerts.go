package log

type alertTarget string
type alertType string

const (
	// AlertTargetTelegram indicates that alerts should be sent to
	// Telegram
	AlertTargetTelegram alertTarget = "telegram"

	// AlertTypeError defines the 'error' level of alerts
	AlertTypeError alertType = "error"

	// AlertTypeInfo defines the 'info' level of alerts
	AlertTypeInfo alertType = "info"

	// EnvKeyAlertTelegramChatId defines the environment variable key
	// where the Telegram chat ID will be retrieved from
	EnvKeyAlertTelegramChatId = "LOG_ALERT_TELEGRAM_CHAT_ID"

	// EnvKeyAlertTelegramToken defines the environment variable key
	// where the Telegram token will be retrieved from
	EnvKeyAlertTelegramToken = "LOG_ALERT_TELEGRAM_TOKEN"
)

const defaultAlertTarget = AlertTargetTelegram
